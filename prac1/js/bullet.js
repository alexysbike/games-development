
var Bullet = function(x, y, enemy){
	enemy || ( enemy = false );
	this.enemy = enemy;
	this.x = x;
	this.y = y;
	this.h = 10;
	this.w = 3;
	this.vel = (canvas.height/2)/90;
	this.alive = true;
};

Bullet.prototype.Initialize = function(x, y, enemy){
	enemy || ( enemy = false );
	this.enemy = enemy;
	this.x = x;
	this.y = y;
	this.alive = true;
}

Bullet.prototype.Logic = function(elapsed){
	if (this.alive) {
		if (this.enemy == true) {
			this.y += this.vel;
			if(this.y > canvas.height){
				this.alive = false;
			}
		} else{
			this.y -= this.vel;
			if(this.y < 0){
				this.alive = false;
			}
		};
	};
};

Bullet.prototype.Render = function(elapsed){
	if (this.alive) {
		ctx.save();

		//Estilos
		ctx.fillStyle = "white";

		//Dibujando Bala
		ctx.fillRect(this.x, this.y, this.w, this.h);

		ctx.restore();
	};
};

Bullet.prototype.Destroy = function(){
	this.alive = false;
};