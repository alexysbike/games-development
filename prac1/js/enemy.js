
var Enemy = function(iX, iY, w, h){
	w || ( w = 30 );
	h || ( h = 30 );

	this.w = w;
	this.h = h;

	iX || ( iX = (canvas.width/2) - (this.w/2)  );
	iY || ( iY = 30 );

	this.x = iX;
	this.y = iY;

	this.hVel = 10;
	this.vVel = 10;
	this.horizontals = 2;
	this.mHorizontals = 6;
	this.ciclos = 0;
	this.mCiclos = 3;

	this.bullets = [];

	this.alive = true;
};

Enemy.prototype.Logic = function (elapsed){
	var length = this.bullets.length;
	for(var i=length;i--;){
		this.bullets[i].Logic(elapsed);
	}
};

Enemy.prototype.Render = function (elapsed){
	if(this.alive == true){
		ctx.save();

		//Estilos
		ctx.fillStyle = "red";
		ctx.strokeStyle = "white";

		//Dibujando player
		ctx.fillRect(this.x, this.y, this.w, this.h);
		
		ctx.restore();
	}
	var length = this.bullets.length;
	for(var i=length;i--;){
		this.bullets[i].Render(elapsed);
	}
};

Enemy.prototype.Shot = function(){
	var nNew = true;
	var length = this.bullets.length;
	for (var i=length;i--;){
		if (this.bullets[i].alive == false) {
			nNew = false;
			this.bullets[i].Initialize(this.x+(this.w/2)-3, this.y+(this.h), true);
			break;
		};
	}
	if(nNew){
		this.bullets.push(new Bullet(this.x+(this.w/2)-3, this.y+(this.h), true));
	}
};

Enemy.prototype.Move = function(max){

	this.x += this.hVel;

	this.horizontals++;

	if (this.horizontals >= this.mHorizontals) {
		this.hVel *= -1;
		this.horizontals = 0;
		this.ciclos++;
	};

	if (this.ciclos >= this.mCiclos){
		this.ciclos = 0;
		this.mCiclos = max;
		this.y += this.vVel;
	}
}

Enemy.prototype.Collide = function (bullet){
	var col = false;   
    if (this.x < bullet.x + bullet.w && this.x + this.w > bullet.x && this.y < bullet.y + bullet.h && this.h + this.y > bullet.y) {
    	this.Destroy();
    	bullet.Destroy();
    	col = true;
	}
	return col;
};

Enemy.prototype.Destroy = function(){
	this.alive = false;
};




