var Game = function (){
	this.player = new Player();
	this.InitializeEnemies();
	this.ebDefaultCooldown = 2;
	this.ebRefresh = 0;
	this.ebCooldown = this.ebDefaultCooldown;
	this.emRefresh = 0;
	this.emCooldown = 1;
	this.state = "gameplay";
	this.overMenu = null;
	this.pauseMenu = null;
	this.tiempo = 0;
};

Game.prototype.Tick = function(elapsed){
	fps.update(elapsed);
	this.Logic(elapsed);
	this.Render(elapsed);
	tracker.refresh();
};

Game.prototype.Logic = function (elapsed){
	InputManager.padUpdate();

	if(this.state === "gameplay"){
		var eLen = this.enemies.length;
		//Pause
		if (InputManager.padPressed & InputManager.PAD.CANCEL)
		{
			this.state = "pause";
			this.Pause();
		}
		
		//Llevando el tiempo transcurrido
		this.tiempo += elapsed;
		//Logica de las Entidades
		this.player.Logic(elapsed);
		for (var i=eLen;i--;){
			this.enemies[i].Logic(elapsed);
		}

		this.emRefresh -= elapsed;
		if (this.emRefresh <= 0) {
			var mCiclo = RandomIntRange(2,5);

			for (var i=eLen;i--;){
				this.enemies[i].Move(mCiclo);
			}
			this.emRefresh = this.emCooldown;
		};

		this.ebRefresh -= elapsed;
		if (this.ebRefresh <= 0 && this.enemies.length > 0) {
			var rIndex;
			do{
				rIndex = RandomInt(this.enemies.length);
			}while(!this.enemies[rIndex].alive)
			this.enemies[rIndex].Shot();
			this.ebRefresh = this.ebCooldown;
		};

		//Deteccion de Colisiones
		//Enemigos
		var pbLen = this.player.bullets.length;
		for (var i=pbLen;i--;){
			if (this.player.bullets[i].alive) {
				for(var j=eLen;j--;){
					if (this.enemies[j].alive) {
						if(this.enemies[j].Collide(this.player.bullets[i])){
							var over = true;
							for (var k=eLen; k--;){
								if (this.enemies[k].alive) {
									over = false;
								};
							}
							if (over) {
								this.state = "gameover";
								this.GameOver();
							};
						};
					};
				}
			};
		}
		//Player
		var bLen;
		for (var i=eLen; i--;){
			if (this.enemies[i].alive) {
				bLen = this.enemies[i].bullets.length;
				for(var j=bLen;j--;){
					if (this.enemies[i].bullets[j].alive && this.player.vidas > 0) {
						if(this.player.Collide(this.enemies[i].bullets[j])){
							if (this.player.vidas <= 0) {
								this.state = "gameover";
								this.GameOver();
							};
						};
					};
				}
			};
		}

		//Aplicando dificultad
		this.ebCooldown -= 0.008*elapsed;
		if (this.ebCooldown < 0.8) {
			this.ebCooldown = 1;
		};
	}
};

Game.prototype.Render = function (elapsed){
	
	//Backgrounds
	ctx.fillRect(0,0,canvas.width,canvas.height);
	
	//Renders
	this.player.Render(elapsed);
	var length = this.enemies.length;
	for (var i=length;i--;){
		this.enemies[i].Render(elapsed);
	}

	//Dibujando los limites de movimiento
	ctx.beginPath();
	ctx.strokeStyle = "white";
	ctx.moveTo((canvas.width/2)-(this.player.dMax/2),0);
	ctx.lineTo((canvas.width/2)-(this.player.dMax/2),canvas.height);
	ctx.moveTo((canvas.width/2)+(this.player.dMax/2),0);
	ctx.lineTo((canvas.width/2)+(this.player.dMax/2),canvas.height);
	ctx.stroke();

	//HUD
	ctx.save();
	ctx.fillStyle = "white";
	ctx.font = "14px sans-serif";
	ctx.textAlign = "left";
	ctx.fillText("Space Invaders", 3, 15);
	ctx.fillText("Vidas: "+this.player.vidas, 3, 100);
	ctx.fillText("Tiempo: "+this.tiempo.toFixed()+"seg", 3, 140);
	ctx.fillText("Instrucciones:", 610, 100);
	ctx.fillText("Movimiento: <-  ->", 610, 140);
	ctx.fillText("Disparo: Barra Espaciadora", 610, 180);
	ctx.fillText("Pausa: Esc", 610, 220);
	ctx.restore();

};

Game.prototype.GameOver = function()
{
	InputManager.reset();
	var bindThis = this;
	this.overMenu = new Menu("Game Over",
			[ "Otra Vez", "Pantalla Principal" ],
			"",
			70, 50, 400,
			function(numItem) {
				if (numItem == 0) { 
						GameLoopManager.run(function(elapsed) { bindThis.Tick(elapsed); }); 
						bindThis.Initialize();
					}
				else if (numItem == 1) StartMenu();
			},
			function(elapsed) { bindThis.Render(elapsed); });
	GameLoopManager.run(function(elapsed) { bindThis.overMenu.Tick(elapsed); });
};

Game.prototype.Pause = function()
{
	InputManager.reset();
	var bindThis = this;
	this.pauseMenu = new Menu("Pausa",
			[ "Continuar", "Otra Vez", "Pantalla Principal" ],
			"",
			70, 50, 400,
			function(numItem) {
				if(numItem == 0){
					GameLoopManager.run(function(elapsed) { bindThis.Tick(elapsed); });
					bindThis.state = "gameplay";
					bindThis.pauseMenu = null;	
				}
				else if (numItem == 1) { 
						GameLoopManager.run(function(elapsed) { bindThis.Tick(elapsed); }); 
						bindThis.Initialize();
					}
				else if (numItem == 2) StartMenu();
			},
			function(elapsed) { bindThis.Render(elapsed); });
	GameLoopManager.run(function(elapsed) { bindThis.pauseMenu.Tick(elapsed); });
};

Game.prototype.Initialize = function(){
	this.state = "gameplay";
	this.tiempo = 0;
	this.ebCooldown = this.ebDefaultCooldown;
	this.player.Initialize();
	this.player.EmptyBullets();
	this.player.vidas = 2;
	this.InitializeEnemies();
	this.overMenu = null;
};

Game.prototype.InitializeEnemies = function  (config) {
	config || ( config = [{enemies:5, type:"normal"},{enemies:6,type:"normal"},{enemies:5,type:"normal"}] );
	this.enemies = new Array();
	var eW,eH,bEnemy=10,line=30,num,iSpot;
	var length = config.length;
	for (var i=length; i--;){
		switch(config[i].type){
			case "normal":
				eW = 30;
				eH = 30;
				break;
		}
		num = config[i].enemies;
		if (num%2 == 0) {
			iSpot = (canvas.width/2)-(num/2)*eW-(((num/2)-0.5)*bEnemy);
		}else{
			iSpot = (canvas.width/2)-Math.floor(num/2)*eW-Math.floor(num/2)*bEnemy-(eW/2);
		};

		for (var j = num; j--;) {
			this.enemies.push(new Enemy(iSpot+(eW*j)+(bEnemy*j),line,eW,eH));
		};

		line += eH+bEnemy;

	}
}