var ElementTracker = function (container){
	this.elements = [];
	this.container = $(container);
};

ElementTracker.prototype.register = function (obj){
	this.elements.push(obj);
};

ElementTracker.prototype.refresh = function (){

	var container = this.container;

	container.html('');

	$.each(this.elements, function(index, element) {
		container.append('<div class="element '+element.name+'">');

		container.append('<p>');

		container.append('<strong>Name:</strong> '+element.name+'<br>');
		container.append('<strong>Properties:</strong><br>');
		container.append('<ul>');
		
		$.each(element.properties, function(index, prop) {
			container.append('<li>'+prop+': '+element.obj[prop]+'</li>');
		});

		container.append('</ul>');

		container.append('</p>');

		container.append('</div>');
	});

};