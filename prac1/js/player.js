

var Player = function(iX, iY){
	this.w = 30;
	this.h = 30;

	iX || ( iX = (canvas.width/2) - (this.w/2)  );
	iY || ( iY = 420 );

	this.x = iX;
	this.y = iY;
	this.vel = (canvas.width/2)/90;
	this.vidas = 2;
	this.dMax = 400;
	this.refresh =0;
	this.cooldown = 1.5;


	this.bullets = [];
	tracker.register({name:"PlayerBulletsPool", obj:this.bullets, properties:["length"]});
};

Player.prototype.Initialize = function(iX, iY){
	iX || ( iX = (canvas.width/2) - (this.w/2)  );
	iY || ( iY = 420 );

	this.x = iX;
	this.y = iY;

}

Player.prototype.EmptyBullets = function(){
	var length = this.bullets.length;
	for(var i=length;i--;){
		this.bullets[i].Destroy();
	}
};

Player.prototype.Logic = function(elapsed){
	if (this.vidas > 0) {
		
		var bLen = this.bullets.length;
		//Movimiento
		/*if (InputManager.PAD.DOWN) {
			this.y += this.vel;
		}
		if (InputManager.PAD.UP) {
			this.y -= this.vel;
		}*/
		if (InputManager.PAD.RIGHT) {
			this.x += this.vel;
			if (this.x > (canvas.width/2)+(this.dMax/2)-this.w) {
				this.x = (canvas.width/2)+(this.dMax/2)-this.w;
			};
		}
		if (InputManager.PAD.LEFT) {
			this.x -= this.vel;
			if (this.x < (canvas.width/2)-(this.dMax/2)) {
				this.x = (canvas.width/2)-(this.dMax/2);
			};
		}

		if (this.refresh > 0) {
			this.refresh -= elapsed;
		}

		//Balas
		if (InputManager.PAD.SHOT && this.refresh <= 0) {
			var nNew = true;
			for (var i=bLen;i--;){
				if (this.bullets[i].alive == false) {
					nNew = false;
					this.bullets[i].Initialize(this.x+(this.w/2)-3, this.y);
					break;
				};
			}
			if(nNew){
				this.bullets.push(new Bullet(this.x+(this.w/2)-3, this.y));
			}
			this.refresh = this.cooldown;
		}
		bLen = this.bullets.length;
		for(var i=bLen;i--;){
			this.bullets[i].Logic(elapsed);
		}
	};
};

Player.prototype.Render = function (elapsed){
	if(this.vidas > 0){
		ctx.save();

		//Estilos
		ctx.fillStyle = "white";
		ctx.strokeStyle = "white";

		//Dibujando player
		ctx.fillRect(this.x, this.y, this.w, this.h);

		var length = this.bullets.length;
		for(var i=length;i--;){
			this.bullets[i].Render(elapsed);
		}

		ctx.restore();
	}
};

Player.prototype.Collide = function (bullet){
	var col = false;   
    if (this.x < bullet.x + bullet.w && this.x + this.w > bullet.x && this.y < bullet.y + bullet.h && this.h + this.y > bullet.y) {
    	this.Die();
    	bullet.Destroy();
    	col = true;
	}
	return col;
};

Player.prototype.Die = function(){
	this.vidas--;
	if (this.vidas>0) {
		this.Initialize();
	};
};