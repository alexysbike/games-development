var canvas, ctx;

var game, fps, tracker, menu;

function StartGame(){
	menu = null;
	game = new Game();
	InputManager.reset();
	GameLoopManager.run(function(elapsed){ game.Tick(elapsed); });	
}

function StartMenu(){
	game = null;
	// Async load audio and start menu when loaded
	InputManager.reset();
	menu = new Menu("Space Invaders",
			[ "Jugar", "Salir" ],
			"Hecho con fines didacticos por Alexys Gonzalez",
			70, 50, 400,
			function(numItem) { 
				if (numItem == 0) {StartGame()}; 
				if (numItem == 1) {Exit()}; 
			},
			null);
	GameLoopManager.run(function(elapsed) { menu.Tick(elapsed); });
}

function Exit () {
	GameLoopManager.stop();
	game = null;
	menu = null;
	GameLoopManager.run(function(elapsed){ Thx(elapsed); });

}

var time=0;
function Thx (elapsed) {
	time += elapsed;
	ctx.fillStyle = "black";
	ctx.fillRect(0,0,canvas.width,canvas.height);
	ctx.textAlign = "center";
	ctx.fillStyle = "White";
	ctx.font = "24px Times New Roman";
	ctx.fillText("Gracias por jugar", canvas.width/2, canvas.height/2);
	if (time>1) {
		GameLoopManager.stop(); delete GameLoopManager;
	};
}

$(document).ready(function(){
	canvas = document.getElementById('screen');
	ctx = canvas.getContext('2d');
	fps = new FPSMeter("fpsmeter", document.getElementById("fpsmeter"));
	tracker = new ElementTracker("#tracker");

	InputManager.connect(document, canvas);
	StartMenu();
});